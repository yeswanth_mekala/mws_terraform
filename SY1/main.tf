provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  features {}
}

################################################################## MYSQL  ##################################################
resource "azurerm_network_security_group" "mysql-nsg" {
  name                = "wms-us1-${var.env}-mysql-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "mysql" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.mysql.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.mysql-nsg.id
}

resource "azurerm_network_interface" "mysql" {
  count               = 2
  name                = "wms-us1-${var.env}-mysql0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["db"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_mysql[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "mysql" {
    count               = 2 
    name                = "wms-us1-${var.env}-mysql0${count.index + 1}"
    computer_name       = "us1-${var.env}-mysql0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["mysql"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.mysql.*.id, count.index)]
    source_image_id = var.rhel_7_ha

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}

#MySQL Data Disks
resource "azurerm_managed_disk" "mysql" {
  count = 2
  name                 = "wms-us1-${var.env}-mysql0${count.index + 1}-disk-0"
  location             = var.location
  create_option        = "Empty"
  disk_size_gb         = 64
  resource_group_name  = var.rg
  storage_account_type = "Premium_LRS"
  tags = var.default_tags
}

resource "azurerm_virtual_machine_data_disk_attachment" "mysql" {
  count              = 2
  virtual_machine_id = element(azurerm_linux_virtual_machine.mysql.*.id, count.index)
  managed_disk_id    = element(azurerm_managed_disk.mysql.*.id, count.index)
  lun                = 0
  caching            = "None"
}


###############################################       PKG      ##################################################
resource "azurerm_network_security_group" "pkg-nsg" {
  name                = "wms-us1-${var.env}-pkg-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "pkg" {
  count                     = 1
  network_interface_id      = element(azurerm_network_interface.pkg.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.pkg-nsg.id
}

resource "azurerm_network_interface" "pkg" {
  count               = 1
  name                = "wms-us1-${var.env}-pkg0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_pkg[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "pkg" {
    count               = 1
    name                = "wms-us1-${var.env}-pkg0${count.index + 1}"
    computer_name       = "us1-${var.env}-pkg0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["pkg"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.pkg.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################       web      ##################################################
resource "azurerm_network_security_group" "web-nsg" {
  name                = "wms-us1-${var.env}-web-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "web" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.web.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.web-nsg.id
}

resource "azurerm_network_interface" "web" {
  count               = 2
  name                = "wms-us1-${var.env}-web0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_web[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "web" {
    count               = 2
    name                = "wms-us1-${var.env}-web0${count.index + 1}"
    computer_name       = "us1-${var.env}-web0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["web"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.web.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################       pns      ##################################################
resource "azurerm_network_security_group" "pns-nsg" {
  name                = "wms-us1-${var.env}-pns-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "pns" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.pns.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.pns-nsg.id
}

resource "azurerm_network_interface" "pns" {
  count               = 2
  name                = "wms-us1-${var.env}-pns0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_pns[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "pns" {
    count               = 2
    name                = "wms-us1-${var.env}-pns0${count.index + 1}"
    computer_name       = "us1-${var.env}-pns0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["pns"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.pns.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       mail      ##################################################
resource "azurerm_network_security_group" "mail-nsg" {
  name                = "wms-us1-${var.env}-mail-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "mail" {
  count                     = 1
  network_interface_id      = element(azurerm_network_interface.mail.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.mail-nsg.id
}

resource "azurerm_network_interface" "mail" {
  count               = 1
  name                = "wms-us1-${var.env}-mail0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_mail[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "mail" {
    count               = 1
    name                = "wms-us1-${var.env}-mail0${count.index + 1}"
    computer_name       = "us1-${var.env}-mail0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["mail"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.mail.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       maint      ##################################################
resource "azurerm_network_security_group" "maint-nsg" {
  name                = "wms-us1-${var.env}-maint-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "maint" {
  count                     = 1
  network_interface_id      = element(azurerm_network_interface.maint.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.maint-nsg.id
}

resource "azurerm_network_interface" "maint" {
  count               = 1
  name                = "wms-us1-${var.env}-maint0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_maint[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "maint" {
    count               = 1
    name                = "wms-us1-${var.env}-maint0${count.index + 1}"
    computer_name       = "us1-${var.env}-maint0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["maint"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.maint.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       app      ##################################################
resource "azurerm_network_security_group" "app-nsg" {
  name                = "wms-us1-${var.env}-app-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "app" {
  count                     = 4
  network_interface_id      = element(azurerm_network_interface.app.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.app-nsg.id
}

resource "azurerm_network_interface" "app" {
  count               = 4
  name                = "wms-us1-${var.env}-app0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_app[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "app" {
    count               = 4
    name                = "wms-us1-${var.env}-app0${count.index + 1}"
    computer_name       = "us1-${var.env}-app0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["app"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.app.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################       app-pkg      ##################################################

resource "azurerm_network_interface_security_group_association" "app_pkg" {
  count                     = 3
  network_interface_id      = element(azurerm_network_interface.app_pkg.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.app-nsg.id
}

resource "azurerm_network_interface" "app_pkg" {
  count               = 3
  name                = "wms-us1-${var.env}-app0${count.index + 5}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_app_pkg[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "app_pkg" {
    count               = 3
    name                = "wms-us1-${var.env}-app0${count.index + 5}"
    computer_name       = "us1-${var.env}-app0${count.index + 5}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["app"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.app_pkg.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       api      ##################################################
resource "azurerm_network_security_group" "api-nsg" {
  name                = "wms-us1-${var.env}-api-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "api" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.api.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.api-nsg.id
}

resource "azurerm_network_interface" "api" {
  count               = 2
  name                = "wms-us1-${var.env}-api0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_api[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "api" {
    count               = 2
    name                = "wms-us1-${var.env}-api0${count.index + 1}"
    computer_name       = "us1-${var.env}-api0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["api"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.api.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       blb      ##################################################
resource "azurerm_network_security_group" "blb-nsg" {
  name                = "wms-us1-${var.env}-blb-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "blb" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.blb.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.blb-nsg.id
}

resource "azurerm_network_interface" "blb" {
  count               = 2
  name                = "wms-us1-${var.env}-blb0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_blb[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "blb" {
    count               = 2
    name                = "wms-us1-${var.env}-blb0${count.index + 1}"
    computer_name       = "us1-${var.env}-blb0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["blb"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.blb.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}



###############################################       mqtt      ##################################################
resource "azurerm_network_security_group" "mqtt-nsg" {
  name                = "wms-us1-${var.env}-mqtt-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "mqtt" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.mqtt.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.mqtt-nsg.id
}

resource "azurerm_network_interface" "mqtt" {
  count               = 2
  name                = "wms-us1-${var.env}-mqtt0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_mqtt[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "mqtt" {
    count               = 2
    name                = "wms-us1-${var.env}-mqtt0${count.index + 1}"
    computer_name       = "us1-${var.env}-mqtt0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["mqtt"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.mqtt.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################       memc      ##################################################
resource "azurerm_network_security_group" "memc-nsg" {
  name                = "wms-us1-${var.env}-memc-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "memc" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.memc.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.memc-nsg.id
}

resource "azurerm_network_interface" "memc" {
  count               = 2
  name                = "wms-us1-${var.env}-memc0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_memc[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "memc" {
    count               = 2
    name                = "wms-us1-${var.env}-memc0${count.index + 1}"
    computer_name       = "us1-${var.env}-memc0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["memc"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.memc.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################       lbx      ##################################################
resource "azurerm_network_security_group" "lbx-nsg" {
  name                = "wms-us1-${var.env}-lbx-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "lbx" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.lbx.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.lbx-nsg.id
}

resource "azurerm_network_interface" "lbx" {
  count               = 2
  name                = "wms-us1-${var.env}-lbx0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_lbx[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "lbx" {
    count               = 2
    name                = "wms-us1-${var.env}-lbx0${count.index + 1}"
    computer_name       = "us1-${var.env}-lbx0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["lbx"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.lbx.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}

###############################################       lapi      ##################################################
resource "azurerm_network_security_group" "lapi-nsg" {
  name                = "wms-us1-${var.env}-lapi-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "lapi" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.lapi.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.lapi-nsg.id
}

resource "azurerm_network_interface" "lapi" {
  count               = 2
  name                = "wms-us1-${var.env}-lapi0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_lapi[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_linux_virtual_machine" "lapi" {
    count               = 2
    name                = "wms-us1-${var.env}-lapi0${count.index + 1}"
    computer_name       = "us1-${var.env}-lapi0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["lapi"]
    admin_username      = "azureuser"
    disable_password_authentication = true
    network_interface_ids = [element(azurerm_network_interface.lapi.*.id, count.index)]
    source_image_id = var.rhel_8

    admin_ssh_key {
        username   = "azureuser"
        public_key = var.public_key
    }

    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }

    tags = var.default_tags

}


###############################################    Windows Servers ##################################################
###############################################       apps         ##################################################
resource "azurerm_network_security_group" "apps-nsg" {
  name                = "wms-us1-${var.env}-apps-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "apps" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.apps.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.apps-nsg.id
}

resource "azurerm_network_interface" "apps" {
  count               = 2
  name                = "wms-us1-${var.env}-apps0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["web"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_apps[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_windows_virtual_machine" "apps" {
    count               = 2
    name                = "wms-us1-${var.env}-apps0${count.index + 1}"
    computer_name       = "us1-${var.env}-apps0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["apps"]
    admin_username      = var.windows_admin_username
    admin_password      = var.windows_admin_password
    network_interface_ids = [element(azurerm_network_interface.apps.*.id, count.index)]
    #source_image_id = var.windows_2022


    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }
    
    source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2022-datacenter"
    version   = "latest"
    }

    tags = var.default_tags

}

###############################################       aapi      ##################################################
resource "azurerm_network_security_group" "aapi-nsg" {
  name                = "wms-us1-${var.env}-aapi-nsg"
  location            = var.location
  resource_group_name = var.rg
  tags = var.default_tags
}

resource "azurerm_network_interface_security_group_association" "aapi" {
  count                     = 2
  network_interface_id      = element(azurerm_network_interface.aapi.*.id, count.index)
  network_security_group_id = azurerm_network_security_group.aapi-nsg.id
}

resource "azurerm_network_interface" "aapi" {
  count               = 2
  name                = "wms-us1-${var.env}-aapi0${count.index + 1}"
  location            = var.location
  resource_group_name = var.rg
  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = var.subnet["app"]
    private_ip_address_allocation = "Static"
    private_ip_address = var.private_ip_aapi[count.index]
  }
  tags = var.default_tags
}

resource "azurerm_windows_virtual_machine" "aapi" {
    count               = 2
    name                = "wms-us1-${var.env}-aapi0${count.index + 1}"
    computer_name       = "us1-${var.env}-aapi0${count.index + 1}"
    resource_group_name = var.rg
    location            = var.location
    size                = var.instance_size["aapi"]
    admin_username      = var.windows_admin_username
    admin_password      = var.windows_admin_password
    network_interface_ids = [element(azurerm_network_interface.aapi.*.id, count.index)]
    #source_image_id = var.windows_2022


    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "StandardSSD_LRS"
    }
  
  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2022-datacenter"
    version   = "latest"
  }

    tags = var.default_tags

}

