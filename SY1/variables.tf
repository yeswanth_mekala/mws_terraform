variable "subscription_id" { default = "ed2187a7-b27f-477f-b4ec-92f6848217fb" }
variable "windows_admin_username" { default = "wmsadmin" }

variable "windows_admin_password" { 
  type = string
  description = "The windows admin password to be used for windows servers"

}

variable "public_key" { default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/VkmM5ph6WKBHEj29xmDKGXdBsJ1M1dX4H+ngOeNCbvJGUqXlumWtWx6mhnK3uET1HLPspkrNQQgiXb3GnzKXO74al83quDt2/k6Yrt0cRrmFMzjasfPx4niQr5eOXSRSVSWEe0/kuvuCMeFH/JVnjTbScBjGMIVNifbncwfrSgrXLjLVMc9qUxu5DoDTA6Ya0/2+PzsQ427n0pzA8YQznjrbHmU9Sl5j1lMTB6a/av50PqxTVx2g00o869zj9preIIMfyMZzguG2heEtrR6/itYuzhVW3xldSDQf1G92vNnI5tT9ecgxb2VE1m9zyRgXfixEhTtl6KiScbMm+fOOVwbOQHj2UhX9a9bkwy2oieZW+7Asa1DgsWZxDkAduHh1/aA3oHadoSMBia7I73rr+u4fqdVzHdBHyVIcPSWSC+QKUXYgqaXzNlehRHHJf6gMly4gWx18SlUNwqEawsbRAYCVfYu3UBccUMv5aHkkTpLsWRoRIvkvxu28NbuUCOU="}

variable  "rhel_7_ha" {
    default = "/subscriptions/00817d09-5325-4ab2-8f85-dc0135170dfd/resourceGroups/UW2-CS-DEVSECOPS-DEV/providers/Microsoft.Compute/galleries/RHEL/images/RHEL_7_HA/versions/7.9.3"
  }

variable  "rhel_8" {
    default = "/subscriptions/00817d09-5325-4ab2-8f85-dc0135170dfd/resourceGroups/UW2-CS-DEVSECOPS-DEV/providers/Microsoft.Compute/galleries/RHEL/images/RHEL_8/versions/8.4.2"
  }

variable  "windows_2022" {
    default = "/subscriptions/00817d09-5325-4ab2-8f85-dc0135170dfd/resourceGroups/UW2-CS-DEVSECOPS-DEV/providers/Microsoft.Compute/galleries/Windows/images/Windows-2022/versions/0.0.1"
  }

variable "env" {
  default = "sb2"
}

variable "location" {
  default = "West US 2"
}

variable "rg" {
  default = "UW2-SB2-WMS-DEV-RG"
}


variable "virtual-network" {
  default = "West US 2"
}

variable "subnet" {
  type = map
  default = {
    "web"  = "/subscriptions/ed2187a7-b27f-477f-b4ec-92f6848217fb/resourceGroups/UW2-SB2-WMS-DEV-RG/providers/Microsoft.Network/virtualNetworks/UW2-SB2-WMS-DEV-RG-vnet/subnets/wms-sb2-www"
    "app" = "/subscriptions/ed2187a7-b27f-477f-b4ec-92f6848217fb/resourceGroups/UW2-SB2-WMS-DEV-RG/providers/Microsoft.Network/virtualNetworks/UW2-SB2-WMS-DEV-RG-vnet/subnets/wms-sb2-app"
    "db" = "/subscriptions/ed2187a7-b27f-477f-b4ec-92f6848217fb/resourceGroups/UW2-SB2-WMS-DEV-RG/providers/Microsoft.Network/virtualNetworks/UW2-SB2-WMS-DEV-RG-vnet/subnets/wms-sb2-db"
  }
}

variable "instance_size" {
  type = map
  default = {
      "mysql" = "Standard_B2s"
      "web" = "Standard_B2ms"
      "pns" = "Standard_B2s"
      "pkg" = "Standard_B2s"
      "mail" = "Standard_B2s"
      "app" = "Standard_F2s_v2"
      "apppkg" = "Standard_F2s_v2"
      "api" = "Standard_B2s"
      "blb" = "Standard_B2s"
      "mqtt" = "Standard_B2s"
      "memc" = "Standard_B2s"
      "maint" = "Standard_B2ms"
      "lbx" = "Standard_B2s"
      "apps" = "Standard_B2s"
      "aapi" = "Standard_B2s"
      "lapi" = "Standard_B2s"
  }
}

variable "private_ip_mysql" {
  type    = list
  default = ["10.63.23.201", "10.63.23.202"]
}

variable "private_ip_web" {
  type    = list
  default = ["10.63.21.201", "10.63.21.202"]
}

variable "private_ip_pns" {
  type    = list
  default = ["10.63.21.211", "10.63.21.212"]
}

variable "private_ip_pkg" {
  type    = list
  default = ["10.63.21.180"]
}

variable "private_ip_mail" {
  type    = list
  default = ["10.63.21.221"]
}

variable "private_ip_maint" {
  type    = list
  default = ["10.63.21.195"]
}

variable "private_ip_app" {
  type    = list
  default = ["10.63.22.201", "10.63.22.202" , "10.63.22.203", "10.63.22.204"]
}

variable "private_ip_app_pkg" {
  type    = list
  default = ["10.63.22.205", "10.63.22.206", "10.63.22.207"]
}

variable "private_ip_api" {
  type    = list
  default = ["10.63.22.211", "10.63.22.212"]
}

variable "private_ip_blb" {
  type    = list
  default = ["10.63.22.226", "10.63.22.227"]
}

variable "private_ip_memc" {
  type    = list
  default = ["10.63.22.231", "10.63.22.232"]
}

variable "private_ip_mqtt" {
  type    = list
  default = ["10.63.22.251", "10.63.22.252"]
}

variable "private_ip_lbx" {
  type    = list
  default = ["10.63.22.215", "10.63.22.216"]
}

variable "private_ip_apps" {
  type    = list
  default = ["10.63.21.191", "10.63.21.192"]
}

variable "private_ip_aapi" {
  type    = list
  default = ["10.63.22.191", "10.63.22.192"]
}

variable "private_ip_lapi" {
  type    = list
  default = ["10.63.22.246", "10.63.22.247"]
}


#Tags
variable "default_tags" { 
    type = map
    default = { 
        EnvironmentName = "SB2" , EnvironmentType = "Non-Production"
  } 
}

